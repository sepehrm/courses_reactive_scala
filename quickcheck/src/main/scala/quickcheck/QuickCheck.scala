package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import scala.math._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  // Heap generator
	def genHeap: Gen[H] = for {
    v <- arbitrary[Int]
    heap <- oneOf(value(empty), genHeap)
  } yield insert(v, heap)

	implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  /* If you insert an element into an empty heap, then find the minimum, 
   * the result should be the element.
   */
  property("findMin") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  /* If you insert an element into an empty heap, then delete the minimum, 
   * the resulting heap should be empty.
   */
  property("deleteMin") = forAll { a: Int =>
    val h = insert(a, empty)
    deleteMin(h) == empty
  }

  /* If you insert any two elements into an empty heap, 
   * finding the minimum of the resulting heap 
   * should get the smallest of the two elements back.
   */
  property("findMin2") = forAll { (h: H, a:Int, b: Int) =>
    val heapMin = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(a, insert(b, h)))== List(heapMin, a ,b).min
  }

  /* Finding a minimum of the melding of any two heaps 
   * should return a minimum of one or the other.
   */
  property("findMin of meld") = forAll { (h: H, g: H) =>
    findMin(meld(h, g)) == min(findMin(h), findMin(g))
  }

  // recursive helper function to get a sorted list of elements in heap 
  def sortedHeap(h: H): List[Int] = {
    if (isEmpty(h)) List() else findMin(h)::sortedHeap(deleteMin(h))
  }

  /* Given any heap, you should get a sorted sequence of elements 
   * when continually finding and deleting minima.
   */
  property("heap sort") = forAll { (h: H) =>
    val minsSorted = sortedHeap(h)
    minsSorted == minsSorted.sorted
  }

  property("findMin, insert, deleteMin") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, deleteMin(h))) == m
  }

  property("sort, meld, deleteMin") = forAll { (h: H, g: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    sortedHeap(meld(h, g)) == sortedHeap(meld(deleteMin(h), insert(m, g)))
  }

}
