/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue
import akka.event.LoggingReceive

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier 'id' to insert an element 'elem' into the tree.
    * The actor at reference 'requester' should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier 'id' to check whether an element 'elem' is present
    * in the tree. The actor at reference 'requester' should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier 'id' to remove the element 'elem' from the tree.
    * The actor at reference 'requester' should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier 'id'.
    * 'result' is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts 'Operation' and 'GC' messages. */
  val normal: Receive = {
    case op : Operation => root ! op
    case GC => {
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context become garbageCollecting(newRoot)
    }
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * 'newRoot' is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case op : Operation => pendingQueue = pendingQueue.enqueue(op)
    case CopyFinished => {
      root = newRoot
      context become runNext
    }
    case _ =>
  }

  def running: Receive = {
    case or: OperationReply =>
      pendingQueue = pendingQueue.tail
      context become runNext
    case op : Operation =>
      pendingQueue = pendingQueue.enqueue(op)
      running
  }

 def runNext: Receive = {
   if (pendingQueue.isEmpty) normal
   else {
     root ! pendingQueue.head
     running
   }
 }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles 'Operation' messages and 'CopyTo' requests. */
  val normal: Receive = {
    case op: Operation => {
      if (op.elem < elem && subtrees.contains(Left)) subtrees(Left) forward op
      else if (op.elem > elem && subtrees.contains(Right)) subtrees(Right) forward op
      else {
        val reply = op match {
          case Insert(requester, id, toInsert) =>
            if (toInsert == elem) removed = false
            else if (toInsert < elem) insert(Left, toInsert, id)
            else if (toInsert > elem ) insert(Right, toInsert, id)
            OperationFinished(id)
          case Contains(requester, id, toLookup) =>
            ContainsResult(id, !removed && elem == toLookup)
          case Remove(requester, id, toRemove) =>
            if (toRemove == elem) removed = true
            OperationFinished(id)
        }
        op.requester ! reply
        self ! reply
      }
    }

    case CopyTo(treeNode) => {
      var expected = subtrees.values.toSet
      if (!removed) {
        expected += self
        treeNode ! Insert(self, elem, elem)
      }
      subtrees.values.foreach(_ ! CopyTo(treeNode))
      checkDone(expected, removed)
    }

    case or: OperationReply => context.parent forward or
  }

  def insert(position: Position, newElem: Int, id: Int) {
    subtrees += position -> context.actorOf(BinaryTreeNode.props(newElem, initiallyRemoved = false), name = "node-" + newElem)
  }

  // optional
  /** 'expected' is the set of ActorRefs whose replies we are waiting for,
    * 'insertConfirmed' tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case CopyFinished => checkDone(expected - sender, insertConfirmed)
    case of: OperationFinished => checkDone(expected - self, true)
  }

  def checkDone(expected: Set[ActorRef], insertConfirmed: Boolean) = {
    if (expected.isEmpty && insertConfirmed) {
      context.parent ! CopyFinished
      context.stop(self)
    } else {
      context become copying(expected, insertConfirmed)
    }
  }

}
