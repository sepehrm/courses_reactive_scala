package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  //
  // to complete with tests for orGate, demux, ...
  //
  test("orGate example") {
    val in1, in2, out = new Wire
    orGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "or 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === true, "or 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "or 3")
  }

  test("orGate2 example") {
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "or2 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === true, "or2 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "or2 3")
  }

  test("demux0 example") {
    val in, out = new Wire
    demux(in, List(), List(out))
    in.setSignal(false)
    run

    assert(out.getSignal === false, "demux 1")

    in.setSignal(true)
    run

    assert(out.getSignal === true, "demux 2")
  }

  test("demux1 example") {
    val in, c0, out0, out1 = new Wire
    demux(in, List(c0), List(out1, out0))

    c0.setSignal(false)
    in.setSignal(false)
    run
    assert(out0.getSignal === false, "demux1 1")
    assert(out1.getSignal === false, "demux1 2")

    in.setSignal(true)
    run
    assert(out0.getSignal === true, "demux1 3")
    assert(out1.getSignal === false, "demux1 4")

    c0.setSignal(true)
    run
    assert(out0.getSignal === false, "demux1 5")
    assert(out1.getSignal === true, "demux1 6")
  }

  test("demux2 example") {
    val in, c0, c1, out0, out1, out2, out3 = new Wire
    demux(in, List(c1, c0), List(out3, out2, out1, out0))

    in.setSignal(false)
    run
    assert(out0.getSignal === false, "demux 1-1")
    assert(out1.getSignal === false, "demux 1-2")
    assert(out2.getSignal === false, "demux 1-3")
    assert(out3.getSignal === false, "demux 1-4")

    in.setSignal(true)
    run
    assert(out0.getSignal === true, "demux 2-1")
    assert(out1.getSignal === false, "demux 2-2")
    assert(out2.getSignal === false, "demux 2-3")
    assert(out3.getSignal === false, "demux 2-4")
    
    c0.setSignal(true)
    run
    assert(out0.getSignal === false, "demux 3-1")
    assert(out1.getSignal === true, "demux 3-2")
    assert(out2.getSignal === false, "demux 3-3")
    assert(out3.getSignal === false, "demux 3-4")

    c0.setSignal(false)
    c1.setSignal(true)
    run
    assert(out0.getSignal === false, "demux 4-1")
    assert(out1.getSignal === false, "demux 4-2")
    assert(out2.getSignal === true, "demux 4-3")
    assert(out3.getSignal === false, "demux 4-4")

    c0.setSignal(true)
    c1.setSignal(true)
    run
    assert(out0.getSignal === false, "demux 5-1")
    assert(out1.getSignal === false, "demux 5-2")
    assert(out2.getSignal === false, "demux 5-3")
    assert(out3.getSignal === true, "demux 5-4")

  }

}
