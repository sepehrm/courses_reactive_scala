package simulations

import math.random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8

    // to complete: additional parameters of simulation
    val transRate: Double = .4
    val prevalenceRate: Double = .01
    val dieRate = 0.25
    val infectedAtStart = List.fill((population * prevalenceRate).toInt)(randomBelow(population))

    val airTraffic = .01
    val reducedMobility = .5
    val vaccination = .05
  }

  import SimConfig._

  val persons: List[Person] = (for (i <- (0 to population)) yield new Person(i)).toList // to complete: construct list of persons

  def neighbours(room: (Int, Int)) = {
    val row = room._1
    val col = room._2
    val left = (row, (((col-1) % roomColumns) + roomColumns) % roomColumns)
    val right = (row, (((col+1) % roomColumns)+ roomColumns) % roomColumns)
    val up = ((((row-1) % roomRows) + roomRows) % roomRows, col)
    val down = ((((row+1) % roomRows) + roomRows) % roomRows, col)
    List(left, right, up, down)
  }

  def personsInRoom(room: (Int, Int)) = persons.filter(p => p.row == room._1 && p.col == room._2)
  def roomIsVisiblyInfectious(room: (Int, Int)) = personsInRoom(room).exists(p => (p.sick || p.dead))
  def roomIsInfectious(room: (Int, Int)) = personsInRoom(room).exists(p => p.infected)

  class Person (val id: Int) {
    var infected = infectedAtStart.contains(id)
    var sick = false
    var immune = false
    var dead = false

    val incubationTime = 6
    val dieTime = 14
    val immuneTime = 16
    val healTime = 18    

    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)

    //
    // to complete with simulation logic
    //

    def moveAction {
      if (!dead) {
        val safeRooms = neighbours(row, col).filter(r => !roomIsVisiblyInfectious(r))
        if (!safeRooms.isEmpty) {
          val newRoom = safeRooms(randomBelow(safeRooms.length))
          row = newRoom._1
          col = newRoom._2
          getInfectedAction
        }
        afterDelay(randomBelow(5)+1)(moveAction)
      }
    }

    def getInfectedAction {
      if (roomIsInfectious((row, col))) {
        if(random <= transRate && !infected) {
          infected = true
          afterDelay(incubationTime)(getSickAction)
        }
      }
    }

    def getSickAction() {
      sick = true
      afterDelay(dieTime-incubationTime)(dieAction)
    }

    def dieAction() {
      if (random <= dieRate) dead = true
      else afterDelay(immuneTime-dieTime)(getImmuneAction)
    }

    def getImmuneAction() {
      sick = false
      immune = true
      afterDelay(healTime-immuneTime)(getNormalAction)
    }

    def getNormalAction() {
      infected = false
      immune = false
    }

    if (infected) {
      afterDelay(incubationTime)(getSickAction)
    }

    afterDelay(randomBelow(5)+1)(moveAction)
  }
}
